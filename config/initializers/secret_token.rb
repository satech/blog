# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = 'fb75894632e982edf3221a3fca48d1e243965814c8b8a1ee3ed6c955706c6684f54f4a02f0bdedbd2896b667fccf770ba9660e41d89495f6b37bb7adf8c32ebc'
